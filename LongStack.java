import java.util.LinkedList;

public class LongStack {

	public static void main(String[] argum) throws CloneNotSupportedException {
	}

	/** magasin ise Linkedlistina. */
	private LinkedList<Long> s;

	public LongStack() {
		s = new LinkedList<Long>();
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		LongStack tmp = new LongStack();
		tmp.s = (LinkedList<Long>) s.clone();
		return tmp;
		// TODO!!! Peab tegema klooni
	}

	public boolean stEmpty() {
		if (s.isEmpty())
			return true;
		else
			return false;
		// TODO!!! T�hja magasini kontroll
	}

	public void push(long a) {
		s.push(a);
		// TODO!!! Peab lisama elemendi a magasini tipuks
	}

	public long pop() {
		long pop = s.pop();
		return pop;
		// TODO!!! Peab tagastama arvu magasini tipust ja selle �ra v�tma
	} // pop

	public void op(String s) {
		long op2 = pop();
		long op1 = pop();
		if (s.equals("+"))
			push(op1 + op2);
		if (s.equals("-"))
			push(op1 - op2);
		if (s.equals("*"))
			push(op1 * op2);
		if (s.equals("/"))
			push(op1 / op2);
		// TODO!!!
	}

	public long tos() {
		long peek = s.peek();
		return peek;
		// TODO!!! Your code here!
	}

	@Override
	public boolean equals(Object o) {
		LinkedList<Long> other = (LinkedList<Long>) ((LongStack) o).s;
		if (other.isEmpty() && s.isEmpty())
			return true;
		if (s.size() == other.size()) {
			for (int i = 0; i < s.size();) {
				if (s.get(i) == other.get(i))
					return true;
				else
					return false;
			}
		}
		return false;
	}

	public static long interpret(String pol) {
		LongStack m = new LongStack();
		if (pol == null || pol == "")
			throw new RuntimeException(
					"InterpretNullorEmpty: sisend null v�i olematu");
		String[] elements = pol.trim().split("\\s+");
		for (int i = 0; i < elements.length; i++) {
			try {
				long num = Long.parseLong(elements[i]);
				m.push(num);
				if (i > 1) {
					if (i == elements.length - 1) {
						throw new RuntimeException(
								"InterpretIllegalArg: sisendis "+ pol+ " number "+ elements[i]+ " on viimasel kohal, viimasel kohal peab olema tehtem�rk");
					}
				}
			} catch (NumberFormatException e) {
				if (i < (elements.length / 2))
					throw new RuntimeException("InterpretUnderflow: sisendis "+ pol + " on enne tehtem�rke liiga v�he numbreid");
				if ((elements[i].equals("+")) || (elements[i].equals("-"))
						|| (elements[i].equals("*"))
						|| (elements[i].equals("/")))
					m.op(elements[i]);
				else
					throw new RuntimeException("InterpretIllegalArg: sisendis "
							+ pol + " element " + elements[i] + " on vigane");
			}
		}
		long vastus = m.pop();
		return vastus; // TODO!!! Your code here!
	}
}